CKEditor BUikitty Buttons -  is an extension to the Drupal 8 CKEditormodule.


REQUIREMENTS

- ckeditor


FEATURES

- Ability to insert a link as a Uikitty button.
- Movable between content without breaking layout.
- All Uikitty styles available
  - btn-link
  - btn-default
  - btn-primary
  - btn-success
  - btn-warning
  - btn-danger
- All Uikitty sizes available
  - btn-xs
  - btn-sm
  - btn-lg
- Link target option.
- Ability to add a left and/or right icon (using FontAwesome).


INSTALLATION

1- Copy the uikitty_buttons folder to your modules directory.
2- Download the plugin from https://github.com/smonetti/btbutton.
3- Place the plugin in the root libraries folder (/libraries).
4- Go to admin/modules and enable the module.
5- Go to admin/config/content/formats and configure the desired profile.
6- Move a button into the Active toolbar.
7- Clear your browser's cache, and a new button will appear in your toolbar.

